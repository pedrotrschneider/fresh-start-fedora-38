# Fresh Start for Fedora 38

## Post installation

Start by editing `/etc/dnf/dnf.conf` by adding these lines at the end

```bash
fastestmirror=True
max_parallel_downloads=10
defaultyes=True
keepcache=True
```

Then, update your system

```bash
sudo dnf update
```

Enable free and non-free repositories on rpm fusion

```bash
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm -y
sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm -y
```

Add flathub source to flatpaks

```bash
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

Change hostname

```bash
sudo hostnamectl set-hostname "greg"
```

Install media codecs

```bash
sudo dnf groupupdate sound-and-video -y
```

Now, to setup Git, set username and email

```bash
git config --global user.name "Pedro Tonini Rosenberg Schneider"
git config --global user.email "pedrotrschneider@gmail.com"
```

Generate ssh keys

```bash
ssh-keygen -t ed25519
```

Then add the ssh keys to GitHub and GitLab

Copy `.desktop` files

```bash
mkdir -p ~/.local/share/applications/
cd ~/.src/fresh-start-fedora-38/applications/
cp * ~/.local/share/applications/
```

Copy config files 

```bash
cd ~/.src/fresh-start-fedora-38/config/
cp -r * ~/.config/
```

Now set the number of workspaces to a fixed 9, and make workspaces span all monitors (this is on settings > multitasking).

Copy the fonts

```bash
mkdir ~/.local/share/fonts
cd ~/.src/fresh-start-fedora-38/fonts/
cp *.ttf ~/.local/share/fonts/
```

## Install applications

### Installing Nvidia drivers

Enalbe RPM Fusion repositories

```bash
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
```

Install the latest Nvidia drivers, cuda drivers and hardware acceleration software

```bash
sudo dnf install -y akmod-nvidia xorg-x11-drv-nvidia-cuda nvidia-vaapi-driver
```

### Installing [pfetch](https://github.com/dylanaraps/pfetch)

Clone the repo and install the application

```
mkdir -p ~/.local/bin/
cd ~/.src/
git clone https://github.com/dylanaraps/pfetch.git
cd pfetch/
ln -sri pfetch ~/.local/bin/
```

### Installing [Digimend Kernel Drivers](https://github.com/DIGImend/digimend-kernel-drivers)

Install dependencies

```bash
sudo dnf install -y "kernel-devel-uname-r = $(uname -r)"
sudo dnf install -y dkms
```

Clone and build the package

```bash
cd ~/.src
git clone https://github.com/DIGImend/digimend-kernel-drivers.git digimend-kernel-drivers
cd digimend-kernel-drivers
sudo make dkms_install
```

### Install VSCode

Add the repository to dnf

```bash
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
sudo dnf check-update
```

Install VSCode

```bash
sudo dnf install code -y
```

### Install Starship Prompt

```bash
curl -sS https://starship.rs/install.sh | sh
```

### Installing from package managers

From dnf:

```bash
sudo dnf install xournalpp neofetch gnome-tweaks fish micro pavucontrol krita blender mpv htop cava fira-code-fonts bat playerctl lsd xdg-user-dirs pipewire pipewire-pulseaudio unzip lm_sensors pipewire-utils python3-pip python3-devel sushi telegram discord meson ninja-build cmake gcc-c++ alacritty adw-gtk3-theme distrobox steam -y
```

Installing from flathub

```bash
flatpak install flathub com.spotify.Client com.github.marktext.marktext com.github.flxzt.rnote com.mattjakeman.ExtensionManager org.gtk.Gtk3theme.adw-gtk3 org.gtk.Gtk3theme.adw-gtk3-dark -y
```

### Installing media codecs

```bash
sudo dnf install gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel
sudo dnf install lame\* --exclude=lame-devel
sudo dnf group upgrade --with-optional Multimedia --allowerasing
```

## GNOME Extensions

Install extensions dependencies

```bash
sudo dnf install libgda libgda-sqlite
```

- Blur My Shell
- Caffeine
- Tiling Assistant
- Pano - Clipboard Manager
- User Themes
- AppIndicator and KStatusNotifierItem Support
- Launch New Instance

Remember to set the theme for legacy applications to `adw-gtk3-dark`

### Install Custom Accent Colors from Git

Clone and install the extension

```bash
cd ~/.src
git clone https://github.com/dimitriskp22/custom-accent-colors.git
cd custom-accent-colors
make && make install
```

Remember to set icon themes and shell themes to the correct values

## Grub theme

Clone the repository and install the theme

```bash
cd ~/.src
git clone https://github.com/vinceliuice/grub2-themes.git
cd grub2-themes
sudo ./install.sh -t stylish
```

Restart the computer for changes to take effect

## Keybindings

To remove all keybindings, run the `clear-keybindings.sh` script.

To set the appropriate keybindings, run the `set-keybindings.sh` script.

Add custom keybindings for the terminal emulator and nautilus.

## Distrobox

### Odin distrobox

Create the distrobox:

```bash
distrobox create -i archlinux:latest -n odin -H ~/distrobox/odin
distrobox enter odin
```

Configure paru:

```bash
sudo pacman -S --needed base-devel
sudo pacman -S git openssh
cd
mkdir .src
cd .src
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
```

To enable colors on `paru`, uncomment the line that says `color` on `/etc/pacman.conf`

Install some programs:

```bash
paru -S micro visual-studio-code-bin fish valgrind
```

Install odin:

```bash
paru -Sy clang odin-git odinls
```

From now on, use ```distrobox enter odin -e fish``` to enter the distrobox
