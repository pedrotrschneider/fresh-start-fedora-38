#! /bin/sh

# Window manager keybindings
dconf write '/org/gnome/desktop/wm/keybindings/activate-window-menu' "['<Alt>space']"
dconf write '/org/gnome/desktop/wm/keybindings/close' "['<Super>q']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-windows' "['<Alt>Tab']"
dconf write '/org/gnome/desktop/wm/keybindings/toggle-maximized' "['<Super>m','<Super>Up']"
dconf write '/org/gnome/mutter/keybindings/toggle-tiled-left' "['<Super>Left']"
dconf write '/org/gnome/mutter/keybindings/toggle-tiled-right' "['<Super>Right']"
dconf write '/org/gnome/desktop/wm/keybindings/minimize' "['<Super><Shift>n']"
dconf write '/org/gnome/shell/keybindings/toggle-overview' "['<Super>Tab', '<Super>space']"

# Workspace management keybindings
# Switching to workspaces
dconf write '/org/gnome/desktop/wm/keybindings/switch-to-workspace-1' "['<Super>1','<Super>KP_End']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-to-workspace-2' "['<Super>2','<Super>KP_Down']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-to-workspace-3' "['<Super>3','<Super>KP_Next']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-to-workspace-4' "['<Super>4','<Super>KP_Left']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-to-workspace-5' "['<Super>5','<Super>KP_Begin']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-to-workspace-6' "['<Super>6','<Super>KP_Right']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-to-workspace-7' "['<Super>7','<Super>KP_Home']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-to-workspace-8' "['<Super>8','<Super>KP_Up']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-to-workspace-9' "['<Super>9','<Super>KP_Page_Up']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-to-workspace-left' "['<Ctrl><Super>left']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-to-workspace-right' "['<Ctrl><Super>right']"
# Moving windows to workspace
dconf write '/org/gnome/desktop/wm/keybindings/move-to-workspace-1' "['<Shift><Super>1','<Shift><Super>KP_End']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-workspace-2' "['<Shift><Super>2','<Shift><Super>KP_Down']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-workspace-3' "['<Shift><Super>3','<Shift><Super>KP_Next']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-workspace-4' "['<Shift><Super>4','<Shift><Super>KP_Left']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-workspace-5' "['<Shift><Super>5','<Shift><Super>KP_Begin']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-workspace-6' "['<Shift><Super>6','<Shift><Super>KP_Right']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-workspace-7' "['<Shift><Super>7','<Shift><Super>KP_Home']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-workspace-8' "['<Shift><Super>8','<Shift><Super>KP_Up']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-workspace-9' "['<Shift><Super>9','<Shift><Super>KP_Page_Up']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-workspace-left' "['<Ctrl><Shift><Super>left']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-workspace-right' "['<Ctrl><Shift><Super>right']"

# Media keys keybindings
# Launching default applications
dconf write '/org/gnome/settings-daemon/plugins/media-keys/calculator' "['<Super>c']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/control-center' "['<Super>s']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/www' "['<Super>b']"
# Session management
dconf write '/org/gnome/settings-daemon/plugins/media-keys/logout' "['<Super>e']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/screensaver' "['<Super>l']"
dconf write '/org/gnome/desktop/wm/keybindings/panel-run-dialog' "['<Super>r']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-input-source' "['<Super><Ctrl>space', 'XF86Keyboard']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-input-source-backward' "['<Super><Ctrl><Shift>space', '<Shift>XF86Keyboard']"
# Accessibility
dconf write '/org/gnome/settings-daemon/plugins/media-keys/magnifier' "['<Super>equal']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/magnifier-zoom-in' "['<Super><Shift>equal']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/magnifier-zoom-out' "['<Super><Shift>minus']"
# Media management
dconf write '/org/gnome/settings-daemon/plugins/media-keys/play' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/pause' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/stop' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/next' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/previous' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/playback-forward' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/playback-rewind' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/playback-repeat' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/playback-random' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/mic-mute' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/volume-up' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/volume-down' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/volume-mute' "['']"

# Shell keybindings
dconf write '/org/gnome/shell/keybindings/focus-active-notification' "['<Super>g']"
dconf write '/org/gnome/shell/keybindings/open-application-menu' "['<Super>a']"
dconf write '/org/gnome/shell/keybindings show-screenshot-ui' "['Print']"
dconf write '/org/gnome/shell/keybindings/show-screen-recording-ui' "['<Alt>Print']"
dconf write '/org/gnome/shell/keybindings/screenshot' "['<Shift>Print']"
dconf write '/org/gnome/shell/keybindings/screenshot-window' "['<Ctrl>Print']"