dconf write '/org/gnome/desktop/wm/keybindings/activate-window-menu' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/close' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/cycle-group' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/cycle-group-backward' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/cycle-panels-backward' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/cycle-panels' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/cycle-windows' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/cycle-windows-backward' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/maximize' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/minimize' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-monitor-left' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-monitor-right' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-monitor-up' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-monitor-down' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-workspace-left' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-workspace-right' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-workspace-down' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-workspace-up' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-workspace-1' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/move-to-workspace-last' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/panel-run-dialog' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-applications' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-applications-backward' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-group-backward' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-group' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-input-source' "['XF86Keyboard']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-input-source-backward' "['<Shift>XF86Keyboard']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-panels' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-panels-backward' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-to-workspace-left' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-to-workspace-right' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-to-workspace-up' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-to-workspace-down' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-to-workspace-1' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/switch-to-workspace-last' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/toggle-maximized' "['']"
dconf write '/org/gnome/desktop/wm/keybindings/unmaximize' "['']"

dconf write '/org/gnome/mutter/keybindings/switch-monitor' "['XF86Display']"
dconf write '/org/gnome/mutter/keybindings/rotate-monitor' "['XF86RotateWindows']"
dconf write '/org/gnome/mutter/keybindings/toggle-tiled-left' "['']"
dconf write '/org/gnome/mutter/keybindings/toggle-tiled-right' "['']"
dconf write '/org/gnome/mutter/wayland/keybindings/restore-shortcuts' "['']"
dconf write '/org/gnome/mutter/wayland/keybindings/switch-to-session-10' "['']"
dconf write '/org/gnome/mutter/wayland/keybindings/switch-to-session-11' "['']"
dconf write '/org/gnome/mutter/wayland/keybindings/switch-to-session-12' "['']"
dconf write '/org/gnome/mutter/wayland/keybindings/switch-to-session-1' "['']"
dconf write '/org/gnome/mutter/wayland/keybindings/switch-to-session-2' "['']"
dconf write '/org/gnome/mutter/wayland/keybindings/switch-to-session-3' "['']"
dconf write '/org/gnome/mutter/wayland/keybindings/switch-to-session-4' "['']"
dconf write '/org/gnome/mutter/wayland/keybindings/switch-to-session-5' "['']"
dconf write '/org/gnome/mutter/wayland/keybindings/switch-to-session-6' "['']"
dconf write '/org/gnome/mutter/wayland/keybindings/switch-to-session-7' "['']"
dconf write '/org/gnome/mutter/wayland/keybindings/switch-to-session-8' "['']"
dconf write '/org/gnome/mutter/wayland/keybindings/switch-to-session-9' "['']"

dconf write '/org/gnome/settings-daemon/plugins/media-keys/battery-status' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/battery-status-static' "['XF86Battery']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/calculator' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/calculator-static' "['XF86Calculator']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/control-center' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/control-center-static' "['XF86Tools']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/increase-text-size' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/decrease-text-size' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/eject' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/eject-static' "['XF86Eject']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/email' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/email-static' "['XF86Mail']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/hibernate' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/hibernate-static' "['XF86Suspend', 'XF86Hibernate']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/home' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/home-static' "['XF86Explorer']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/keyboard-brightness-toggle' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/keyboard-brightness-toggle-static' "['XF86KbdLightOnOff']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/keyboard-brightness-up' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/keyboard-brightness-up-static' "['XF86KbdBrightnessUp']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/keyboard-brightness-down' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/keyboard-brightness-down-static' "['XF86KbdBrightnessDown']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/logout' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/magnifier' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/magnifier-zoom-in' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/magnifier-zoom-out' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/media' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/media-static' "['XF86AudioMedia']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/mic-mute' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/mic-mute-static' "['XF86AudioMicMute']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/play' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/play-static' "['XF86AudioPlay', '<Ctrl>XF86AudioPlay']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/pause' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/pause-static' "['XF86AudioPause']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/stop' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/stop-static' "['XF86AudioStop']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/next' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/next-static' "['XF86AudioNext', '<Ctrl>XF86AudioNext']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/previous' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/previous-static' "['XF86AudioPrev', '<Ctrl>XF86AudioPrev']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/playback-forward' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/playback-forward-static' "['XF86AudioForward']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/playback-rewind' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/playback-rewind-static' "['XF86AudioRewind']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/playback-repeat' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/playback-repeat-static' "['XF86AudioRepeat']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/playback-random' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/playback-random-static' "['XF86AudioRandomPlay']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/on-screen-keyboard' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/power' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/power-static' "['XF86PowerOff']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/rfkill' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/rfkill-static' "['XF86WLAN', 'XF86UWB', 'XF86RFKill']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/rfkill-bluetooth' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/rfkill-bluetooth-static' "['XF86Bluetooth']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/rotate-video-lock' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/rotate-video-lock-static' "['XF86RotationLockToggle']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/screen-brightness-up' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/screen-brightness-up-static' "['XF86MonBrightnessUp']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/screen-brightness-down' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/screen-brightness-down-static' "['XF86MonBrightnessDown']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/screen-brightness-cycle' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/screen-brightness-cycle-static' "['XF86MonBrightnessCycle']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/screenreader' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/screensaver' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/screensaver-static' "['XF86ScreenSaver']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/search' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/search-static' "['XF86Search']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/suspend' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/suspend-static' "['XF86Sleep']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/toggle-contrast' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/touchpad-toggle' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/touchpad-toggle-static' "['XF86TouchpadToggle', '<Ctrl><Super>XF86TouchpadToggle']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/touchpad-on' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/touchpad-on-static' "['XF86TouchpadOn']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/touchpad-off' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/touchpad-off-static' "['XF86TouchpadOff']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/volume-up' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/volume-up-static' "['XF86AudioRaiseVolume', '<Ctrl>XF86AudioRaiseVolume']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/volume-up-quiet' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/volume-up-quiet-static' "['<Alt>XF86AudioRaiseVolume', '<Alt><Ctrl>XF86AudioRaiseVolume']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/volume-up-precise' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/volume-up-precise-static' "['<Shift>XF86AudioRaiseVolume', '<Ctrl><Shift>XF86AudioRaiseVolume']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/volume-down' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/volume-down-static' "['XF86AudioLowerVolume', '<Ctrl>XF86AudioLowerVolume']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/volume-down-quiet' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/volume-down-precise' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/volume-down-quiet-static' "['<Alt>XF86AudioLowerVolume', '<Alt><Ctrl>XF86AudioLowerVolume']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/volume-down-precise-static' "['<Shift>XF86AudioLowerVolume', '<Ctrl><Shift>XF86AudioLowerVolume']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/volume-mute' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/volume-mute-static' "['XF86AudioMute']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/volume-mute-quiet' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/volume-mute-quiet-static' "['<Alt>XF86AudioMute']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/www' "['']"
dconf write '/org/gnome/settings-daemon/plugins/media-keys/www-static' "['XF86WWW']"

dconf write '/org/gnome/shell/extensions/apps-menu/apps-menu-toggle-menu' "['']"

dconf write '/org/gnome/shell/keybindings/focus-active-notification' "['']"
dconf write '/org/gnome/shell/keybindings/open-application-menu' "['']"
dconf write '/org/gnome/shell/keybindings/show-screenshot-ui' "['']"
dconf write '/org/gnome/shell/keybindings/show-screen-recording-ui' "['']"
dconf write '/org/gnome/shell/keybindings/screenshot' "['']"
dconf write '/org/gnome/shell/keybindings/screenshot-window' "['']"
dconf write '/org/gnome/shell/keybindings/shift-overview-down' "['']"
dconf write '/org/gnome/shell/keybindings/shift-overview-up' "['']"
dconf write '/org/gnome/shell/keybindings/switch-to-application-1' "['']"
dconf write '/org/gnome/shell/keybindings/switch-to-application-2' "['']"
dconf write '/org/gnome/shell/keybindings/switch-to-application-3' "['']"
dconf write '/org/gnome/shell/keybindings/switch-to-application-4' "['']"
dconf write '/org/gnome/shell/keybindings/switch-to-application-5' "['']"
dconf write '/org/gnome/shell/keybindings/switch-to-application-6' "['']"
dconf write '/org/gnome/shell/keybindings/switch-to-application-7' "['']"
dconf write '/org/gnome/shell/keybindings/switch-to-application-8' "['']"
dconf write '/org/gnome/shell/keybindings/switch-to-application-9' "['']"
dconf write '/org/gnome/shell/keybindings/toggle-application-view' "['']"
dconf write '/org/gnome/shell/keybindings/toggle-message-tray' "['']"
dconf write '/org/gnome/shell/keybindings/toggle-overview' "['']"